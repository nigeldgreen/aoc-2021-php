<?php

require __DIR__.'/../vendor/autoload.php';

ini_set('xdebug.max_nesting_level', 10000);

$day = new App\Lib\Classes\Days\Day_10('day_10.txt');
$day->render();