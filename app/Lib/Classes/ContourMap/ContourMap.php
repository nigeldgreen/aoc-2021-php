<?php

namespace App\Lib\Classes\ContourMap;

use Illuminate\Support\Collection;

class ContourMap
{
    public Collection $map;
    public Collection $basins;

    public function __construct(Collection $input)
    {
        $this->map = $input->map( function ($line) {
            return collect(str_split($line))
                ->map( function ($element) {
                   return intval($element);
                });
        });
        $this->basins = collect();
        $this->findLowPoints();
    }

    public function lowPoints() : Collection
    {
        return $this->basins->map(function (Basin $basin) {
            return $basin->lowPoint->value;
        });
    }

    public function findLowPoints() : void
    {
        $this->map->each(function ($row, $yIndex) {
            $row->each(function ($element, $xIndex) use ($yIndex) {
                $point = new ContourPoint($element, [$xIndex, $yIndex]);
                if ($this->isLowPoint($point, $this->comparatorsFor($point))) {
                    $this->basins->push(new Basin($point));
                }
            });
        });
    }

    public function mapBasins()
    {
        $this->basins = $this->basins->each(function (Basin $basin) {
            $this->followContour($basin, $basin->lowPoint);
        });
    }

    public function followContour(Basin $basin, ContourPoint $point) : void
    {
        $basin->points->push($point);
        $comparators = $this->comparatorsFor($point);
        $comparators->each(function (ContourPoint $point) use ($basin) {
            if ($point->value !== 9 && ! $basin->points->contains($point)) {
                $this->followContour($basin, $point);
            }
        });
    }

    public function comparatorsFor(ContourPoint $point) : Collection
    {
        $comparators = collect();
        $x = $point->x;
        $y = $point->y;

        isset($this->map[$y - 1][$x])
            ? $comparators->push(new ContourPoint($this->map[$y - 1][$x], [$x, $y - 1]))
            : $comparators->push(new ContourPoint(9));

        isset($this->map[$y][$x - 1])
            ? $comparators->push(new ContourPoint($this->map[$y][$x - 1], [$x - 1, $y]))
            : $comparators->push(new ContourPoint(9));

        isset($this->map[$y][$x + 1])
            ? $comparators->push(new ContourPoint($this->map[$y][$x + 1], [$x + 1, $y]))
            : $comparators->push(new ContourPoint(9));

        isset($this->map[$y + 1][$x])
            ? $comparators->push(new ContourPoint($this->map[$y + 1][$x], [$x, $y + 1]))
            : $comparators->push(new ContourPoint(9));

        return $comparators;
    }

    public function isLowPoint(ContourPoint $point, Collection $comparators) : bool
    {
        return $comparators->every(function ($comparator) use ($point) {
            return $comparator->value > $point->value;
        });
    }
}