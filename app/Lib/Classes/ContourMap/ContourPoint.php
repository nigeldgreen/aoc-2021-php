<?php

namespace App\Lib\Classes\ContourMap;

class ContourPoint
{
    public int $value;
    public int|null $x;
    public int|null $y;

    /**
     * Constructor for ContourPoint class
     *
     * When we create the coordinates for the point, use 9 for any values not set as this is the 'terminator'
     * character for a grid.
     *
     * @param int $value
     * @param array $coordinates
     */
    public function __construct(int $value, array $coordinates = [])
    {
        $this->value = $value;
        $this->x = $coordinates !== [] ? $coordinates[0] : 9;
        $this->y = $coordinates !== [] ? $coordinates[1] : 9;
    }
}