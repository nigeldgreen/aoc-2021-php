<?php

namespace App\Lib\Classes\ContourMap;

use Illuminate\Support\Collection;
use JetBrains\PhpStorm\Pure;

class Basin
{
    public ContourPoint $lowPoint;
    public Collection $points;

    public function __construct(ContourPoint $lowPoint)
    {
        $this->lowPoint = $lowPoint;
        $this->points = collect();
    }

    #[Pure]
    public function getSize() : int
    {
        return $this->points->count();
    }
}