<?php

namespace App\Lib\Classes;

use Illuminate\Support\Collection;

class CrabFleet
{
    public Collection $allPositions;
    public Collection $occupiedPositions;
    public array $cumulativeSums;
    public int $mostEfficientFuelCost = 0;
    public int $mostEfficientPosition = 0;

    /**
     * Constructor for CrabFleet class
     *
     * Create a new fleet from the input string by exploding it, converting all values to integers and then returning
     * the subset of unique values, in ascending order.
     *
     * @param string $positions
     */
    public function __construct(string $positions = "")
    {
        $this->occupiedPositions =
            collect(explode(',', $positions))
            ->map(function ($position) {
                return intval($position);
            })
            ->sort()
            ->values();

        $this->allPositions = collect(array_fill(0, $this->occupiedPositions->max() + 1, 0))
            ->map(function ($item, $index) {
                return $index;
            });

        $previous = 0;
        $this->cumulativeSums = $this->allPositions->map(function ($item, $index) use (&$previous) {
            if ($index === 0) {
                return 0;
            }
            $previous = $index + $previous;
            return $previous;
        })->toArray();
    }

    public function reset()
    {
        $this->mostEfficientFuelCost = 0;
        $this->mostEfficientPosition = 0;
    }

    public function fuelCostOfJump(int $end, $exponential) : int
    {
        return $this->occupiedPositions
            ->map(function ($position) use ($end, $exponential) {
                $distance = abs($position - $end);
                return $exponential ? $this->cumulativeSums[$distance] : $distance;
            })
                ->sum();
    }

    public function calculateMostEfficientPosition($exponential = false)
    {
        $this->allPositions->each(function ($position) use ($exponential) {
            $fuelCost = $this->fuelCostOfJump($position, $exponential);
            if ($this->mostEfficientFuelCost === 0 || $fuelCost < $this->mostEfficientFuelCost) {
                $this->mostEfficientFuelCost = $fuelCost;
                $this->mostEfficientPosition = $position;
            }
        });
    }

    public function calculateMostEfficientPositionLinear()
    {
        $this->calculateMostEfficientPosition();
    }

    public function calculateMostEfficientPositionExponential()
    {
        $this->calculateMostEfficientPosition(true);
    }
}