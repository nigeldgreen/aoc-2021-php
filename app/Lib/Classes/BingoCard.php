<?php

namespace App\Lib\Classes;

use Illuminate\Support\Collection;

class BingoCard
{
    public Collection $numbers;

    public function setNumbers(array $numbers) : BingoCard
    {
        $this->numbers = collect($numbers)->map(function ($number) {
            return $number;
        });
        return $this;
    }

    public function isWinner() : bool
    {
        $lines = array_merge($this->getRows(), $this->getColumns());
        foreach ($lines as $line) {
            if (collect($line)->every(fn($element) => $element === 'x')) {
                return true;
            }
        }
        return false;
    }

    public function sumUnmarkedNumbers()
    {
        return $this->numbers->filter(function ($number) {
            return $number !== 'x';
        })->sum();
    }

    public function getRows() : array
    {
        return $this->numbers
            ->sliding(5, 5)
            ->map(function($item) {
                return $item->values();
            })
            ->toArray();
    }

    public function getColumns() : array
    {
        $columns = [];
        for($i = 0; $i < 5; $i++) {
            $columns[] = $this->numbers
                ->nth(5, $i)
                ->toArray();
        }
        return $columns;
    }

    public function markCard(int $number)
    {
        $this->numbers = $this->numbers->map(function ($item) use ($number) {
            return $item == $number ? 'x' : $item;
        });
    }
}