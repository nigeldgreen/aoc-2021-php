<?php

namespace App\Lib\Classes\Grid;

use Illuminate\Support\Collection;

class Line
{
    public Collection $points;
    public Point $start;
    public Point $end;
    public string $type;

    public function __construct(string $points)
    {
        $this->getStartAndEndCoordinates($points);
        $this->points = collect();
        $this->calculatePoints();
    }

    /**
     * Split the string into coordinates and make sure the line is always increasing
     *
     * We want to make sure that all input is stored with int values, and that horizontal lines go from left to
     * right, while vertical lines go from top to bottom.
     *
     * @param string $points
     * @return void
     */
    public function getStartAndEndCoordinates(string $points) : void
    {
        $parts = explode(' ', $points);

        $first = Point::getPointFromString($parts[0]);
        $last = Point::getPointFromString($parts[2]);

        list($this->start, $this->end) = Point::orderPoints($first, $last);
    }

    /**
     * @return void
     */
    public function calculatePoints() : void
    {
        $x = $this->start->x;
        $y = $this->start->y;

        if ($x === $this->end->x) {
            $this->type = 'vertical';
            $this->getPointsForVerticalLine($x, $y);
        } elseif ($y === $this->end->y) {
            $this->type = 'horizontal';
            $this->getPointsForHorizontalLine($x, $y);
        } else {
            $this->type = 'diagonal';
            $this->getPointsForDiagonalLine($x, $y);
        }
    }

    private function getPointsForVerticalLine(int $x, int $y)
    {
        while ($y <= $this->end->y) {
            $this->points->push(Point::getPointFromArray([$x, $y]));
            $y++;
        }
    }

    private function getPointsForHorizontalLine(int $x, int $y)
    {
        while ($x <= $this->end->x) {
            $this->points->push(Point::getPointFromArray([$x, $y]));
            $x++;
        }
    }

    private function getPointsForDiagonalLine(int $x, int $y)
    {
        if ($y < $this->end->y) {
            while ($x <= $this->end->x) {
                $this->points->push(Point::getPointFromArray([$x, $y]));
                $x++;
                $y++;
            }
        } else {
            while ($x <= $this->end->x) {
                $this->points->push(Point::getPointFromArray([$x, $y]));
                $x++;
                $y--;
            }
        }
    }
}