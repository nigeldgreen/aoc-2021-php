<?php

namespace App\Lib\Classes\Grid;

use Illuminate\Support\Collection;

class Point
{
    public int $x;
    public int $y;
    public Collection $coordinates;

    private function __construct(array $points)
    {
        $this->x = $points[0];
        $this->y = $points[1];
        $this->coordinates = collect([$this->x, $this->y]);
    }

    /**
     * Split the string into coordinates and return a point object with int coordinates
     *
     * @param string $points
     * @return Point
     */
    public static function getPointFromString(string $points) : Point
    {
        $points = explode(',', $points);
        return new static([intval($points[0]), intval($points[1])]);
    }

    /**
     * Split an array into coordinates and return a point object with int coordinates
     *
     * @param array $points
     * @return Point
     */
    public static function getPointFromArray(array $points) : Point
    {
        return new static([intval($points[0]), intval($points[1])]);
    }

    /**
     * Return coordinates so they are ordered
     *
     * If points make a horizontal or diagonal line then return with lowest x-coordinate first
     * If points make a vertical line then return with lowest y-coordinate first
     *
     * @param Point $p1
     * @param Point $p2
     *
     * @return array
     */
    public static function orderPoints(Point $p1, Point $p2) : array
    {
        if ($p1->x === $p2->x) {
            return $p1->y < $p2->y ? [$p1, $p2] : [$p2, $p1];
        } else {
            return $p1->x < $p2->x ? [$p1, $p2] : [$p2, $p1];
        }
    }
}