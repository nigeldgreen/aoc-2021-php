<?php

namespace App\Lib\Classes\Grid;

use JetBrains\PhpStorm\Pure;

class Grid
{
    public int $size;
    public array $grid;

    #[Pure]
    public function __construct(int $size)
    {
        $this->size = $size;
        $this->grid = $this->makeEmptyGrid();
    }

    public function makeEmptyGrid() : array
    {
        return array_fill(0, $this->size, array_fill(0, $this->size, 0));
    }

    public function setGrid(array $grid) : void
    {
        $this->grid = $grid;
    }

    public function render() : array
    {
        return $this->grid;
    }

    public function update(Line $line)
    {
        $line->points->each(function ($point) {
            $this->grid[$point->y][$point->x]++;
        });
    }

    public function calculateOverlap() : int
    {
        $overlaps = 0;
        foreach ($this->grid as $row) {
            $overlaps = $overlaps + collect($row)->filter( function ($point) {
                return $point >= 2;
            })->count();
        }
        return $overlaps;
    }
}