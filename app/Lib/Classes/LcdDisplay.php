<?php

namespace App\Lib\Classes;

use Illuminate\Support\Collection;

class LcdDisplay
{
    public Collection $inputs;
    public Collection $outputs;
    public Collection $mappedInputs;
    public Collection $mappedOutputs;

    public function __construct(string $line)
    {
        $split = explode(' | ', $line);
        $this->inputs = collect(explode(' ', $split[0]))
            ->map(fn($jumbled) => $this->orderedString($jumbled));
        $this->outputs = collect(explode(' ', $split[1]))
            ->map(fn($jumbled) => $this->orderedString($jumbled));
        $this->mappedInputs = collect();
        $this->mappedOutputs = collect();
        $this->mapOutputs();
    }

    public function uniqueNumberOutputs() : int
    {
        return $this->outputs->map(function (string $number) {
            $length = strlen($number);
            return in_array($length, [2, 3, 4, 7]) ? 1 : 0;
        })->sum();
    }

    public function mapUniqueOutputs() : void
    {
        $this->inputs->each(function (string $number) {
            switch (strlen($number)) {
                case 2:
                    $this->mappedOutputs[1] = $number;
                    break;
                case 3:
                    $this->mappedOutputs[7] = $number;
                    break;
                case 4:
                    $this->mappedOutputs[4] = $number;
                    break;
                case 7:
                    $this->mappedOutputs[8] = $number;
                    break;
            }
        });
    }

    public function mapOutputs()
    {
        // First, map all the numbers that have unique representations
        $this->mapUniqueOutputs();

        // Get collections of chars for all the numbers we know about
        $one = collect(str_split($this->mappedOutputs[1]));
        $four = collect(str_split($this->mappedOutputs[4]));
        $seven = collect(str_split($this->mappedOutputs[7]));
        $eight = collect(str_split($this->mappedOutputs[8]));

        // Calculate 3 as the 5-letter input that contains all the characters of 7
        $this->mappedOutputs[3] = $this->inputs->filter(function ($input) use ($seven) {
            if (strlen($input) == 5) {
                return collect(str_split($input))->diff($seven)->count() === 2;
            }
            return false;
        })->first();
        $three = collect(str_split($this->mappedOutputs[3]));

        // map 'a' by finding the element in 7 that's missing in 1
        $this->mappedInputs['a'] = $seven->diff($one)->first();

        // map 'd' by intersecting 3 minus the letters in 7 with 4 minus the letters in 7
        $this->mappedInputs['d'] =
            $three->diff($seven)->intersect($four->diff($seven))->first();

        // map 'g' by finding the difference of 3 minus the letters in 7 and 4 minus the letters in 7
        $this->mappedInputs['g'] =
            $three->diff($seven)->diff($four->diff($seven))->first();

        // 'b' is the letter in 4 that is not in 3 or 7
        $this->mappedInputs['b'] =
            $four->diff($seven)->diff($three)->first();

        // 'e' is the letter in 8 that is not in 3, 4 or 7
        $this->mappedInputs['e'] =
            $eight->diff($seven)->diff($three)->diff($four)->first();

        // we can now map 5 as the 5-letter input that contains the mapped inputs for a, b, d and g
        $mapped = collect([
            $this->mappedInputs['a'],
            $this->mappedInputs['b'],
            $this->mappedInputs['d'],
            $this->mappedInputs['g'],
        ]);
        $this->mappedOutputs[5] =
            $this->inputs->filter(function ($input) use ($mapped) {
                if (strlen($input) == 5) {
                    return collect(str_split($input))->diff($mapped)->count() === 1;
                }
                return false;
            })->first();
        $five = collect(str_split($this->mappedOutputs[5]));

        // 'c' is the letter in 5 that's not in 7
        $this->mappedInputs['c'] =
            $seven->diff($five)->first();

        // we can now map 2 as the 5-letter input that contains the mapped inputs for a, c, d, e and g
        $two = collect([
            $this->mappedInputs['a'],
            $this->mappedInputs['c'],
            $this->mappedInputs['d'],
            $this->mappedInputs['e'],
            $this->mappedInputs['g'],
        ]);
        $this->mappedOutputs[2] = implode($two->toArray());

        // 'f' is the letter in 2 that's not in 7
        $this->mappedInputs['f'] =
            $seven->diff($two)->first();

        // Now map 0, 6 and 9 as we know all the mapped letters
        $this->mappedOutputs[0] = implode([
            $this->mappedInputs['a'],
            $this->mappedInputs['b'],
            $this->mappedInputs['c'],
            $this->mappedInputs['e'],
            $this->mappedInputs['f'],
            $this->mappedInputs['g'],
        ]);

        $this->mappedOutputs[6] = implode([
            $this->mappedInputs['a'],
            $this->mappedInputs['b'],
            $this->mappedInputs['d'],
            $this->mappedInputs['e'],
            $this->mappedInputs['f'],
            $this->mappedInputs['g'],
        ]);

        $this->mappedOutputs[9] = implode([
            $this->mappedInputs['a'],
            $this->mappedInputs['b'],
            $this->mappedInputs['c'],
            $this->mappedInputs['d'],
            $this->mappedInputs['f'],
            $this->mappedInputs['g'],
        ]);

        $this->mappedOutputs = $this->mappedOutputs
            ->map(fn($jumbled) => $this->orderedString($jumbled))
            ->flip();
    }

    public function getTotalOutput() : string
    {
        $output = $this->outputs->map(function ($output) {
            return $this->mappedOutputs->get($output);
        })->toArray();

        return intval(implode('', $output));
    }

    public function orderedString(string $jumbled) : string
    {
        return implode(collect(str_split($jumbled))
            ->sort()
            ->toArray());
    }
}