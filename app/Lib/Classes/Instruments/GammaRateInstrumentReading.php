<?php namespace App\Lib\Classes\Instruments;

use App\Lib\Contracts\InstrumentInterface;

class GammaRateInstrumentReading extends AbstractInstrumentReading implements InstrumentInterface
{
    public function getRawInstrumentReading(): string
    {
        return $this->processLinesByBit();
    }
}