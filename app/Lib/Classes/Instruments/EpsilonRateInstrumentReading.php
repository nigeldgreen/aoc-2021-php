<?php namespace App\Lib\Classes\Instruments;

use App\Lib\Contracts\InstrumentInterface;

class EpsilonRateInstrumentReading extends AbstractInstrumentReading implements InstrumentInterface
{
    public function getRawInstrumentReading(): string
    {
        return $this->flipBits($this->processLinesByBit());
    }
}