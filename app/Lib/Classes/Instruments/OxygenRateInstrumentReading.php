<?php namespace App\Lib\Classes\Instruments;

use App\Lib\Contracts\InstrumentInterface;
use App\Lib\Exceptions\NoNumbersLeftException;

class OxygenRateInstrumentReading extends AbstractInstrumentReading implements InstrumentInterface
{
    /**
     * @throws NoNumbersLeftException
     */
    public function getRawInstrumentReading(): string
    {
        return $this->reduceInputToSingleValue();
    }
}