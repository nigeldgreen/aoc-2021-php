<?php namespace App\Lib\Classes\Instruments;

use App\Lib\Contracts\InstrumentInterface;
use App\Lib\Exceptions\NoNumbersLeftException;

class Co2RateInstrumentReading extends AbstractInstrumentReading implements InstrumentInterface
{
    /**
     * @throws NoNumbersLeftException
     */
    public function getRawInstrumentReading(): string
    {
        return $this->reduceInputToSingleValue(true);
    }
}