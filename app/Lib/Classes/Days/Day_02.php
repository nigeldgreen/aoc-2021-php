<?php namespace App\Lib\Classes\Days;

class Day_02 extends Day
{

    /**
     * Constructor for Day_01 class
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        parent::__construct("Day Two", $filename);
    }

    function partOne(): string
    {
        $x = $y = 0;
        $this->input->each( function($line) use (&$x, &$y) {
            list($direction, $magnitude) = explode(" ", $line);
            if ($direction === "forward") {
                $x += $magnitude;
            }
            if ($direction === "down") {
                $y += $magnitude;
            }
            if ($direction === "up") {
                $y -= $magnitude;
            }
        });
        return $x * $y;
    }

    function partTwo(): string
    {
        $x = $y = $aim = 0;
        $this->input->each(function ($line) use (&$x, &$y, &$aim) {
            list($direction, $magnitude) = explode(" ", $line);
            if ($direction === "forward") {
                $x += $magnitude;
                $y += $aim * $magnitude;
            }
            if ($direction === "down") {
                $aim += $magnitude;
            }
            if ($direction === "up") {
                $aim -= $magnitude;
            }
        });
        return $x * $y;
    }
}