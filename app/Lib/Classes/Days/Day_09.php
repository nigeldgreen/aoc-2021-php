<?php namespace App\Lib\Classes\Days;

use App\Lib\Classes\ContourMap\Basin;
use App\Lib\Classes\ContourMap\ContourMap;

class Day_09 extends Day
{
    public ContourMap $map;

    /**
     * Constructor for Day_09 class
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        parent::__construct("Day Nine", $filename);
        $this->map = new ContourMap($this->input);
    }

    function partOne() : string|null
    {
        return $this->map->basins
            ->map(function(Basin $basin) {
                return $basin->lowPoint->value + 1;
            })->sum();
    }

    function partTwo() : string|null
    {
        $this->map->mapBasins();
        return $this->map->basins->map(function (Basin $basin) {
            return $basin->getSize();
        })
            ->sortDesc()
            ->take(3)
            ->reduce(function ($carry, $item) {
                return $carry * $item;
            }, 1);
    }
}