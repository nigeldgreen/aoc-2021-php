<?php namespace App\Lib\Classes\Days;

use App\Lib\Classes\BingoCard;
use App\Lib\Exceptions\NoNumbersLeftException;
use Illuminate\Support\Collection;

class Day_04 extends Day
{
    public Collection $numbers;
    public Collection $bingoCards;
    public int $currentNumber;
    public BingoCard|null $winningCard = null;

    /**
     * Constructor for Day_04 class
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        parent::__construct("Day Four", $filename);
        $this->numbers = collect(explode(',', $this->input[0]));
        $this->bingoCards = collect();
        $this->getBingoCards();
    }

    function partOne(): string
    {
        try {
            while ($this->winningCard === null) {
                $this->callNumber();
                $this->checkForWinners();
            }
            return $this->winningCard->sumUnmarkedNumbers() * $this->currentNumber;
        }
        catch (NoNumbersLeftException) {
            return "There were no winners";
        }
    }

    function partTwo(): string
    {
        try {
            while ($this->bingoCards->count() > 0) {
                $this->callNumber();
                $this->checkForWinners();
            }
            return $this->winningCard->sumUnmarkedNumbers() * $this->currentNumber;
        }
        catch (NoNumbersLeftException) {
            return "There were no winners";
        }
    }

    /**
     * @throws NoNumbersLeftException
     */
    public function callNumber()
    {
        if ($this->numbers->count() > 0) {
            $this->currentNumber = $this->numbers->shift();
            $this->bingoCards->each(function (BingoCard $card) {
                $card->markCard($this->currentNumber);
            });
        } else {
            throw new NoNumbersLeftException();
        }
    }

    public function checkForWinners()
    {
        $this->bingoCards->each(function (BingoCard $card) {
            if ($card->isWinner()) {
                $this->winningCard = $card;
                $this->bingoCards = $this->bingoCards->reject(function(BingoCard $card) {
                    return $card->isWinner();
                });
            }
        });
    }

    /**
     * Generate bingo cards for the input given
     *
     * Clean up the input lines to get rid of formatting spaces and build a nested array containing
     * a representation of all the cards that will be in play.
     *
     * @return void
     */
    public function getBingoCards() : void
    {
        $numbers = [];
        $this->input->slice(1)->each(function ($line) use (&$numbers) {
            if ($line === "") {
                if (count($numbers) > 0) {
                    $this->addBingoCard($numbers);
                    $numbers = [];
                }
            } else {
                $line = preg_replace('/^ /', '', $line);
                $line = str_replace('  ', ' ', $line);
                $numbers = array_merge($numbers, explode(' ', $line));
            }
        });
        $this->addBingoCard($numbers);
    }

    private function addBingoCard(array $numbers) :void
    {
        $card = new BingoCard();
        $card->setNumbers($numbers);
        $this->bingoCards->push($card);
    }
}