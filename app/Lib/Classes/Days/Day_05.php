<?php namespace App\Lib\Classes\Days;

use App\Lib\Classes\Grid\Grid;
use App\Lib\Classes\Grid\Line;
use Illuminate\Support\Collection;

class Day_05 extends Day
{
    public int $gridsize;
    public Collection $lines;

    /**
     * Constructor for Day_05 class
     * @param string $filename
     * @param int $gridsize
     */
    public function __construct(string $filename, int $gridsize = 1000)
    {
        parent::__construct("Day Five", $filename);
        $this->lines = $this->setLines();
        $this->gridsize = $gridsize;
    }

    public function setLines() : Collection
    {
        return $this->input->map(function ($line) {
            return new Line($line);
        });
    }

    function partOne(): int
    {
        $grid = new Grid($this->gridsize);
        $this->lines
            ->filter(function(Line $line) {
                return $line->type !== 'diagonal';
            })
            ->each(function(Line $line) use ($grid) {
                $grid->update($line);
            });
        return $grid->calculateOverlap();
    }

    function partTwo(): string
    {
        $grid = new Grid($this->gridsize);
        $this->lines
            ->each(function($line) use ($grid) {
            $grid->update($line);
        });
        return $grid->calculateOverlap();
    }
}