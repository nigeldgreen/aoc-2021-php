<?php namespace App\Lib\Classes\Days;

use App\Lib\Traits\ManageFileInput;
use Illuminate\Support\Collection;

abstract class Day
{
    use ManageFileInput;

    public string $title;
    public string $filename;
    public Collection $input;

    public function __construct($title, $filename)
    {
        $this->title = $title;
        $this->filename = $filename;
        $this->input = $this->getInputAsCollection();
    }

    public function render()
    {
        echo "Results for " . $this->title . "\n\n";

        $partOne = $this->partOne();
        echo "Part one result:\n";
        if ($partOne) {
            $start = microtime(true);
            echo $partOne . "\n";
            $end = microtime(true);
            echo "Completed in " . $end - $start . " seconds\n\n";
        } else {
            echo "Not yet implemented\n\n";
        }

        $partTwo = $this->partTwo();
        echo "Part two result:\n";
        if ($partTwo) {
            $start = microtime(true);
            echo $partTwo . "\n";
            $end = microtime(true);
            echo "Completed in " . $end - $start . " seconds\n\n";
        } else {
            echo "Not yet implemented\n\n";
        }
    }

    abstract function partOne();
    abstract function partTwo();
}