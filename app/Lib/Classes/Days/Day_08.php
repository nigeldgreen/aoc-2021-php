<?php namespace App\Lib\Classes\Days;

use App\Lib\Classes\CrabFleet;
use App\Lib\Classes\LcdDisplay;
use Illuminate\Support\Collection;

class Day_08 extends Day
{
    public Collection $displays;

    /**
     * Constructor for Day_08 class
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        parent::__construct("Day Eight", $filename);

        $this->displays = $this->input->map(function ($line) {
            return new LcdDisplay($line);
        });
    }

    function partOne() : string
    {
        return $this->displays->map(function ($display) {
            return $display->uniqueNumberOutputs();
        })->sum();
    }

    function partTwo() : string
    {
        $test = $this->displays->map(function ($display) {
            return $display->getTotalOutput();
        });
        return $test->sum();
    }
}