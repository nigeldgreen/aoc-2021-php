<?php namespace App\Lib\Classes\Days;

use App\Lib\Classes\LanternfishPopulation;

class Day_06 extends Day
{
    public LanternfishPopulation $population;

    /**
     * Constructor for Day_06 class
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        parent::__construct("Day Six", $filename);
        $this->population = new LanternfishPopulation($this->input[0]);
    }

    function partOne(): int
    {
        $this->population->newDay(80);
        return $this->population->count();
    }

    function partTwo(): string
    {
        $this->population->populate();
        $this->population->newDay(256);
        return $this->population->count();
    }
}