<?php namespace App\Lib\Classes\Days;

use Illuminate\Support\Collection;

class Day_01 extends Day
{
    /**
     * Constructor for Day_01 class
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        parent::__construct("Day One", $filename);
    }

    /**
     * @return int|string
     */
    function partOne() : int|string
    {
        return $this->getIncrease();
    }

    /**
     * Send a sliding window of three input entries to the getIncrease function
     *
     * Take three entries from the input ata a time and sum them to get the collection to pass in to
     * the getIncrease function.
     *
     * @return int|string
     */
    function partTwo(): int|string
    {
        return $this->getIncrease(
            $this->input->sliding(3)->map(fn($item) => $item->sum())
        );
    }

    /**
     * Calculate how many times the elements in a sequence increase over the previous element
     *
     * @param Collection|null $input
     * @return int
     */
    public function getIncrease(Collection $input = null) : int
    {
        $input = $input ?? $this->input;
        $return = $input->sliding()->map(function ($item) {
            return $item->shift() < $item->shift() ? 1 : 0;
        });
        return $return->sum();
    }
}