<?php namespace App\Lib\Classes\Days;

use App\Lib\Classes\CrabFleet;

class Day_07 extends Day
{
    public CrabFleet $fleet;

    /**
     * Constructor for Day_07 class
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        parent::__construct("Day Seven", $filename);
        $this->fleet = new CrabFleet($this->input[0]);
    }

    function partOne() : string
    {
        $this->fleet->calculateMostEfficientPositionLinear();
        return "Most efficient position is position "
            . $this->fleet->mostEfficientPosition
            . " which has a fuel cost of "
            . $this->fleet->mostEfficientFuelCost;
    }

    function partTwo() : string
    {
        $this->fleet->reset();
        $this->fleet->calculateMostEfficientPositionExponential();
        return "Most efficient position is position "
            . $this->fleet->mostEfficientPosition
            . " which has a fuel cost of "
            . $this->fleet->mostEfficientFuelCost;
    }
}