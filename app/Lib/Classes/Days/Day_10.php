<?php namespace App\Lib\Classes\Days;

use App\Lib\Classes\Navigation\Subsystem;

class Day_10 extends Day
{
    public Subsystem $subsystem;

    /**
     * Constructor for Day_10 class
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        parent::__construct("Day Ten", $filename);
    }

    function partOne() : string|null
    {
        $this->subsystem = (new Subsystem($this->input))
            ->validateLines();

        return $this->subsystem->errorScore;
    }

    function partTwo() : string|null
    {
        $this->subsystem = (new Subsystem($this->input))
            ->validateLines()
            ->completeLines();

        return $this->subsystem->completionScore;
    }
}