<?php namespace App\Lib\Classes\Days;

use App\Lib\Classes\Instruments\Co2RateInstrumentReading;
use App\Lib\Classes\Instruments\EpsilonRateInstrumentReading;
use App\Lib\Classes\Instruments\GammaRateInstrumentReading;
use App\Lib\Classes\Instruments\OxygenRateInstrumentReading;
use App\Lib\Contracts\InstrumentInterface;
use App\Lib\Exceptions\NoNumbersLeftException;
use Exception;

class Day_03 extends Day
{
    public InstrumentInterface $gammaRateInstrument;
    public InstrumentInterface $epsilonRateInstrument;
    public InstrumentInterface $oxygenRateInstrument;
    public InstrumentInterface $co2RateInstrument;

    /**
     * Constructor for Day_03 class
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        parent::__construct("Day Three", $filename);
        $this->gammaRateInstrument = new GammaRateInstrumentReading($this->input);
        $this->epsilonRateInstrument = new EpsilonRateInstrumentReading($this->input);
        $this->oxygenRateInstrument = new OxygenRateInstrumentReading($this->input);
        $this->co2RateInstrument = new Co2RateInstrumentReading($this->input);
    }

    function partOne(): string
    {
        try {
            $gamma = $this->gammaRateInstrument->getInstrumentReading();
            $epsilon = $this->epsilonRateInstrument->getInstrumentReading();
            return $gamma * $epsilon;
        } catch (Exception) {
            return "there was an unexpected error...";
        }
    }

    function partTwo(): string
    {
        $oxygen = $this->oxygenRateInstrument->getInstrumentReading();
        $co2 = $this->co2RateInstrument->getInstrumentReading();
        return $oxygen * $co2;
    }
}