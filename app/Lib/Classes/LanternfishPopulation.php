<?php

namespace App\Lib\Classes;

use Illuminate\Support\Collection;

class LanternfishPopulation
{
    public string $input;
    public Collection $fish;

    public function __construct(string $input = "")
    {
        $this->input = $input;
        $this->populate();
    }

    /**
     * Populate the instance with new data from the given input
     *
     * @return void
     */
    public function populate() : void
    {
        $this->refresh();

        if ($this->input !== "") {
            $timers = explode(',', $this->input);
            foreach ($timers as $timer) {
                $this->add(intval($timer));
            }
        }
    }

    /**
     * Populate the instance with an empty array
     */
    public function refresh() : void
    {
        $this->fish = collect(array_fill(0, 9, 0));
    }

    /**
     * Add a new fish top the population with the given timer
     *
     * @param int $timer
     */
    public function add(int $timer) : void
    {
        $this->fish[$timer] = $this->fish[$timer] + 1;
    }

    /**
     * Return the total number of lanternfish in the population
     *
     * @return int
     */
    public function count() : int
    {
        return $this->fish->sum();
    }

    /**
     * Update the population with changes for a number of days
     *
     * As time goes on the timers count down for the population and eligible lanternfish reproduce. We model this
     * by shifting the collection, adding new fish (index 8) and resetting the timer on fish that have just spawned.
     *
     * @param int $times
     * @return void
     */
    public function newDay(int $times = 1) : void
    {
        collect()->times($times, function() {
            $zeroes = $this->fish->shift();
            $this->fish = $this->fish->values();
            $this->fish[6] = $this->fish->get(6) + $zeroes;
            $this->fish[8] = $this->fish->get(8) + $zeroes;
        });
    }
}