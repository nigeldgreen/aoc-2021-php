<?php

namespace App\Lib\Classes;

use Illuminate\Support\Collection;

class FileHandler
{
    private string $filename;
    private array $lines;

    /**
     * Store the file associated with the instance and initialise the return array
     *
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        $this->filename = $filename;
        $this->lines = [];
        return $this;
    }

    /**
     * Read lines from the associated file and return as an array
     *
     * @return Collection
     */
    public function getInputAsCollection() : Collection
    {
        $handle = @fopen($this->filename, "r");
        if ($handle) {
            while(!feof($handle)) {
                $this->lines[] = rtrim(fgets($handle));
            }
            fclose($handle);
        }
        $key = array_key_last($this->lines);
        if (isset($this->lines[$key]) && $this->lines[$key] == "") {
            array_pop($this->lines);
        }

        return collect($this->lines);
    }
}