<?php

namespace App\Lib\Classes\Navigation;

use Illuminate\Support\Collection;

class Subsystem
{
    public Collection $input;
    public int $errorScore = 0;
    public int $completionScore;

    /**
     * @param Collection $input
     */
    public function __construct(Collection $input)
    {
        $this->input = $input->map(function ($line) {
            return new SubsystemLine($line);
        });
    }

    /**
     * Validate input lines to detect if any are invalid
     *
     * Invalid lines will have an incorrectly matching closing bracket. When any invalid character is found, the
     * calculated value is added to the error total and the line is removed from the input.
     *
     * @return $this
     */
    public function validateLines() : Subsystem
    {
        $this->input = $this->input->filter(function (SubsystemLine $line) {
            $lineScore = $this->calculateErrorValue($line->validate());
            $this->errorScore += $lineScore;

            return $lineScore === 0;
        })->values();

        return $this;
    }

    /**
     * Find the median of all valid completion lines
     *
     * Each line calculates its score and this method then returns the middle one. There will always be an odd number
     * of lines that are valid so we can rely on there being an integer value for the median.
     *
     * @return $this
     */
    public function completeLines() : Subsystem
    {
        $this->completionScore = $this->input->map(function (SubsystemLine $line) {
            $line->complete();
            return $line->completionScore;
        })
            ->median();

        return $this;
    }

    /**
     * For each type of closing bracket, return the value to use in the error calculation
     *
     * @param string $char
     * @return int
     */
    public function calculateErrorValue(string $char) : int
    {
        return match ($char) {
            ')' => 3,
            ']' => 57,
            '}' => 1197,
            '>' => 25137,
            default => 0,
        };
    }
}