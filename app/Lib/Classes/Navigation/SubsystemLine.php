<?php

namespace App\Lib\Classes\Navigation;

use Illuminate\Support\Collection;

class SubsystemLine
{
    public string $input;
    public Collection $openers;
    public int $completionScore = 0;

    const OPENERS = ['[', '{', '(', '<'];
    const CLOSERS = [']', '}', ')', '>'];

    /**
     * @param string $input
     */
    public function __construct(string $input = '')
    {
        $this->input = $input;
        $this->errors = collect();
        $this->openers = collect();
    }

    /**
     * Validate the line; return the first invalid character or empty string (if line is valid)
     *
     * @return string
     */
    public function validate() : string
    {
        foreach (str_split($this->input) as $char) {
            if (in_array($char, self::OPENERS)) $this->openers->push($char);
            if (in_array($char, self::CLOSERS)) {
                if (! $this->bracketMatch($this->openers->pop(), $char)) {
                    return $char;
                }
            }
        }
        return '';
    }

    /**
     * Calculate the completion score for a valid line
     *
     * The completion score is calculated by multiplying the current score by 5 and then adding on the increment for the
     * completion character. Openers are closed in the opposite order to when they are opened, so reverse the collection
     * first to make sure we calculate in the correct order.
     *
     * @return void
     */
    public function complete() : void
    {
        $this->openers
            ->reverse()
            ->each(function ($char) {
                $this->completionScore = $this->completionScore * 5 + $this->calculateCompletionValue($char);
            });
    }

    /**
     * Check that a pair of brackets match
     *
     * Only return true if the open and close brackets match, in the correct order
     *
     * @param string $open
     * @param string $close
     * @return bool
     */
    public function bracketMatch(string $open, string $close) : bool
    {
        if (! in_array($open, self::OPENERS) || ! in_array($close, self::CLOSERS)) {
            return false;
        }
        if ($open === '(') {
            return $close === ')';
        }
        if ($open === '[') {
            return $close === ']';
        }
        if ($open === '{') {
            return $close === '}';
        }
        if ($open === '<') {
            return $close === '>';
        }

        return false;
    }

    /**
     * For each type of closing bracket, return the value to use in the completion calculation
     *
     * @param string $char
     * @return int
     */
    public function calculateCompletionValue(string $char) : int
    {
        return match ($char) {
            '(' => 1,
            '[' => 2,
            '{' => 3,
            '<' => 4,
            default => 0,
        };
    }
}