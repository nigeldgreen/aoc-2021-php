<?php namespace App\Lib\Traits;

use App\Lib\Classes\FileHandler;
use Illuminate\Support\Collection;

Trait ManageFileInput {
    /**
     * Get the full path for a passed-in filename
     *
     * Abstracting this to a trait so we have a single place to define the directory holding the input files
     *
     * @return string
     */
    private function getFilepath() : string
    {
        return realpath(__DIR__.'/../../../input') . '/' . $this->filename;
    }

    /**
     * Get a new FileHandler and use it to build an array of the file input
     *
     * @return Collection
     */
    public function getInputAsCollection() : Collection
    {
        $reader = new FileHandler($this->getFilepath());
        return $reader->getInputAsCollection();
    }
}