<?php namespace App\Lib\Contracts;

use Illuminate\Support\Collection;

interface InstrumentInterface
{
    public function getInstrumentReading() : int;
    public function reduceInputToSingleValue(bool $invert) : string;
    public function processLinesByBit(Collection $lines = null, bool $strict = false) : string;
    public function flipBits(string $number) : string;
}