<?php namespace Tests\Days;

use App\Lib\Classes\Days\Day_10;
use App\Lib\Classes\Navigation\Subsystem;
use App\Lib\Classes\Navigation\SubsystemLine;
use PHPUnit\Framework\TestCase;

class Day10Test extends TestCase
{
    // Tests for the Navigation\SubsystemLine class
    /** @test */
    public function can_match_each_bracket_correctly()
    {
        $subsystemLine = new SubsystemLine();

        // correct matches return true
        $this->assertTrue($subsystemLine->bracketMatch('[', ']'));
        $this->assertTrue($subsystemLine->bracketMatch('{', '}'));
        $this->assertTrue($subsystemLine->bracketMatch('(', ')'));
        $this->assertTrue($subsystemLine->bracketMatch('<', '>'));

        // incorrect matches return false
        $this->assertFalse($subsystemLine->bracketMatch('(', '>'));
        $this->assertFalse($subsystemLine->bracketMatch('(', '('));

        // incorrect order returns false
        $this->assertFalse($subsystemLine->bracketMatch(')', '('));

        // incorrect data returns false
        $this->assertFalse($subsystemLine->bracketMatch('$', 'this should throw an error!!'));
    }
    /** @test */
    public function can_validate_single_element_chunk()
    {
        $subsystemLine = new SubsystemLine('[]');
        $this->assertEquals('', $subsystemLine->validate());
    }
    /** @test */
    public function can_find_syntax_error_in_single_element_chunk()
    {
        $subsystemLine = new SubsystemLine('[}');
        $this->assertEquals('}', $subsystemLine->validate());
    }
    /** @test */
    public function can_validate_nested_chunk()
    {
        $subsystemLine = new SubsystemLine('[[<[([])]<([[{}[[()]]]');
        $this->assertEquals('', $subsystemLine->validate());
    }
    /** @test */
    public function can_find_syntax_error_in_nested_chunk()
    {
        $subsystemLine = new SubsystemLine('[[<[([]))<([[{}[[()]]]');
        $this->assertEquals(')', $subsystemLine->validate());
    }



    // Tests for the Navigation\Subsystem class
    /** @test */
    public function can_calculate_error_value_for_bracket_correctly()
    {
        $sys = new Subsystem(collect());

        $this->assertEquals(3, $sys->calculateErrorValue(')'));
        $this->assertEquals(57, $sys->calculateErrorValue(']'));
        $this->assertEquals(1197, $sys->calculateErrorValue('}'));
        $this->assertEquals(25137, $sys->calculateErrorValue('>'));
    }
    /** @test */
    public function can_calculate_completion_value_for_bracket_correctly()
    {
        $subsystemLine = new SubsystemLine('');

        $this->assertEquals(1, $subsystemLine->calculateCompletionValue('('));
        $this->assertEquals(2, $subsystemLine->calculateCompletionValue('['));
        $this->assertEquals(3, $subsystemLine->calculateCompletionValue('{'));
        $this->assertEquals(4, $subsystemLine->calculateCompletionValue('<'));
    }
    /** @test */
    public function invalid_lines_removed_from_input_after_validation()
    {
        $sys = new Subsystem(collect([
            '[]', // valid
            '[[<[([])]<([[{}[[()]]]', // valid
            '{]', // invalid
            '[[<[([]))<([[{}[[()]]]', // invalid
            '<>', // valid
        ]));

        $sys->validateLines();

        $this->assertEquals('[]', $sys->input[0]->input);
        $this->assertEquals('[[<[([])]<([[{}[[()]]]', $sys->input[1]->input);
        $this->assertEquals('<>', $sys->input[2]->input);
    }
    /** @test */
    public function can_calculate_correct_score_for_single_line()
    {
        $subsystemLine = new SubsystemLine('<{[(');

        $subsystemLine->validate();
        $subsystemLine->complete();

        $this->assertEquals(194, $subsystemLine->completionScore);
    }
    /** @test */
    public function can_calculate_correct_score_for_whole_subsystem()
    {
        $sys = new Subsystem(collect([
            '(((',
            '((',
            '(',
            '(((((',
            '((((',
        ]));

        $sys->validateLines();
        $sys->completeLines();

        $this->assertEquals(31, $sys->completionScore);
    }



    // Tests for the main day class
    /** @test */
    public function can_get_correct_test_result_for_part_one()
    {
        $day = new Day_10('day_10_test.txt');
        $this->assertEquals(26397, $day->partOne());
    }
    /** @test */
    public function can_get_correct_test_result_for_part_two()
    {
        $day = new Day_10('day_10_test.txt');
        $this->assertEquals(288957, $day->partTwo());
    }
}