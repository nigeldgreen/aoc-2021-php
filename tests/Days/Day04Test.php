<?php namespace Tests\Days;

use App\Lib\Classes\BingoCard;
use App\Lib\Classes\Days\Day_04;
use App\Lib\Exceptions\NoNumbersLeftException;
use PHPUnit\Framework\TestCase;

class Day04Test extends TestCase
{
    protected Day_04 $day;

    /** @test */
    public function can_get_numbers_from_input()
    {
        $this->day = new Day_04('day_04_test.txt');
        $this->assertEquals(
            collect([7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1]),
            $this->day->numbers
        );
    }
    /** @test */
    public function can_set_numbers_for_bingo_card()
    {
        $card = new BingoCard();
        $card->setNumbers([22,13,17,11,0,8,2,23,4,24,21,9,14,16,7,6,10,3,18,5,1,12,20,15,19]);

        $this->assertEquals(
            collect([22,13,17,11,0,8,2,23,4,24,21,9,14,16,7,6,10,3,18,5,1,12,20,15,19]),
            $card->numbers
        );
    }
    /** @test */
    public function can_get_correct_rows_for_bingo_card()
    {
        $card = new BingoCard();
        $card->setNumbers([22,13,17,11,0,8,2,23,4,24,21,9,14,16,7,6,10,3,18,5,1,12,20,15,19]);

        $this->assertEquals(
            [
                [22,13,17,11,0],
                [8,2,23,4,24],
                [21,9,14,16,7],
                [6,10,3,18,5],
                [1,12,20,15,19],
            ],
            $card->getRows()
        );
    }
    /** @test */
    public function can_get_correct_columns_for_bingo_card()
    {
        $card = new BingoCard();
        $card->setNumbers([22,13,17,11,0,8,2,23,4,24,21,9,14,16,7,6,10,3,18,5,1,12,20,15,19]);

        $this->assertEquals(
            [
                [22,8,21,6,1],
                [13,2,9,10,12],
                [17,23,14,3,20],
                [11,4,16,18,15],
                [0,24,7,5,19],
            ],
            $card->getColumns()
        );
    }
    /** @test */
    public function can_update_bingo_card_from_called_number()
    {
        $card = new BingoCard();
        $card->setNumbers([22,13,17,11,0,8,2,23,4,24,21,9,14,16,7,6,10,3,18,5,1,12,20,15,19]);

        $card->markCard(7);
        $this->assertEquals(
            collect([22,13,17,11,0,8,2,23,4,24,21,9,14,16,'x',6,10,3,18,5,1,12,20,15,19]),
            $card->numbers
        );

        $card->markCard(14);
        $this->assertEquals(
            collect([22,13,17,11,0,8,2,23,4,24,21,9,'x',16,'x',6,10,3,18,5,1,12,20,15,19]),
            $card->numbers
        );
    }
    /** @test */
    public function can_identify_card_with_winning_line()
    {
        $card = new BingoCard();
        $card->setNumbers([22,13,17,11,0,'x','x','x','x','x',21,9,14,16,7,6,10,3,18,5,1,12,20,15,19]);

        $this->assertTrue($card->isWinner());
    }
    /** @test */
    public function can_identify_card_with_winning_column()
    {
        $card = new BingoCard();
        $card->setNumbers([22,13,'x',11,0,8,2,'x',4,24,21,9,'x',16,7,6,10,'x',18,5,1,12,'x',15,19]);

        $this->assertTrue($card->isWinner());
    }
    /** @test */
    public function can_sum_of_unmarked_numbers_on_winning_card()
    {
        $card = new BingoCard();
        $card->setNumbers(['x','x','x','x','x',8,2,'x',4,24,'x',9,'x',16,7,'x',10,'x',18,'x',1,12,'x',15,19]);

        $this->assertEquals(145, $card->sumUnmarkedNumbers());
    }
    /** @test */
    public function can_get_bingo_cards_from_input()
    {
        $this->day = new Day_04('day_04_test.txt');

        $this->assertEquals(
            collect([22,13,17,11,0,8,2,23,4,24,21,9,14,16,7,6,10,3,18,5,1,12,20,15,19]),
            $this->day->bingoCards[0]->numbers
        );
        $this->assertEquals(
            collect([3,15,0,2,22,9,18,13,17,5,19,8,7,25,23,20,11,10,24,4,14,21,16,12,6]),
            $this->day->bingoCards[1]->numbers
        );
        $this->assertEquals(
            collect([14,21,17,24,4,10,16,15,9,19,18,8,23,26,20,22,11,13,6,5,2,0,12,3,7]),
            $this->day->bingoCards[2]->numbers
        );
    }
    /** @test */
    public function calling_number_removes_number_from_start_of_array()
    {
        $this->day = new Day_04('day_04_test.txt');
        try {
            $this->day->callNumber();
            $this->assertEquals(
                collect([4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1]),
                $this->day->numbers
            );
        } catch (NoNumbersLeftException $e) {
            $this->fail("We won't get here!");
        }
    }
    /** @test */
    public function bingo_cards_correct_after_first_number_called()
    {
        $this->day = new Day_04('day_04_test.txt');
        try {
            $this->day->callNumber();

            $this->assertEquals(7, $this->day->currentNumber);

            $this->assertEquals(
                collect([22,13,17,11,0,8,2,23,4,24,21,9,14,16,'x',6,10,3,18,5,1,12,20,15,19]),
                $this->day->bingoCards[0]->numbers
            );
            $this->assertEquals(
                collect([3,15,0,2,22,9,18,13,17,5,19,8,'x',25,23,20,11,10,24,4,14,21,16,12,6]),
                $this->day->bingoCards[1]->numbers
            );
            $this->assertEquals(
                collect([14,21,17,24,4,10,16,15,9,19,18,8,23,26,20,22,11,13,6,5,2,0,12,3,'x']),
                $this->day->bingoCards[2]->numbers
            );
        } catch (NoNumbersLeftException $e) {
            $this->fail("We won't get here!");
        }
    }
    /** @test */
    public function can_get_correct_answer_for_test_data_part_one()
    {
        $this->day = new Day_04('day_04_test.txt');
        $this->assertEquals(4512, $this->day->partOne());
    }
    /** @test */
    public function can_get_correct_answer_for_test_data_part_two()
    {
        $this->day = new Day_04('day_04_test.txt');
        $this->assertEquals(1924, $this->day->partTwo());
    }
}