<?php

namespace Tests\Days;
use App\Lib\Classes\Days\Day_03;
use App\Lib\Exceptions\NoNumbersLeftException;
use PHPUnit\Framework\TestCase;

class Day03Test extends TestCase
{
    protected Day_03 $day;

    public function setUp() : void
    {
        $this->day = new Day_03('day_03_test.txt');
    }
    /** @test */
    public function can_calculate_gamma_rate_for_test_data()
    {
        $this->assertEquals('10110', $this->day->gammaRateInstrument->getRawInstrumentReading());
    }
    /** @test */
    public function can_calculate_epsilon_rate_for_test_data()
    {
        $this->assertEquals('01001', $this->day->epsilonRateInstrument->getRawInstrumentReading());
    }
    /** @test */
    public function can_calculate_oxygen_generator_rating_for_test_data()
    {
        try {
            $this->assertEquals("10111", $this->day->oxygenRateInstrument->getRawInstrumentReading());
        } catch (NoNumbersLeftException $e) {
            $this->fail("We won't get here!");
        }
    }
    /** @test */
    public function can_calculate_co2_scrubber_rating_for_test_data()
    {
        try {
            $this->assertEquals("01010", $this->day->co2RateInstrument->getRawInstrumentReading());
        } catch (NoNumbersLeftException $e) {
            $this->fail("We won't get here!");
        }
    }
    /** @test */
    public function can_get_correct_answer_for_test_data_part_one()
    {
        $this->assertEquals(198, $this->day->partOne());
    }
    /** @test */
    public function can_get_correct_answer_for_test_data_part_two()
    {
        $this->assertEquals(230, $this->day->partTwo());
    }
}