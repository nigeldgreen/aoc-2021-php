<?php namespace Tests\Days;

use App\Lib\Classes\ContourMap\Basin;
use App\Lib\Classes\ContourMap\ContourMap;
use App\Lib\Classes\ContourMap\ContourPoint;
use App\Lib\Classes\Days\Day_09;
use App\Lib\Traits\ManageFileInput;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;

class Day09Test extends TestCase
{
    use ManageFileInput;

    protected string $filename;
    private Collection $testGrid;
    private ContourPoint $topLeft;
    private ContourPoint $topRight;
    private ContourPoint $bottomLeft;
    private ContourPoint $bottomRight;

    protected function setUp(): void
    {
        parent::setUp();
        $this->filename = 'day_09_test.txt';
        $this->testGrid = collect([
            collect([2, 1, 9, 9, 9, 4, 3, 2, 1, 0]),
            collect([3, 9, 8, 7, 8, 9, 4, 9, 2, 1]),
            collect([9, 8, 5, 6, 7, 8, 9, 8, 9, 2]),
            collect([8, 7, 6, 7, 8, 9, 6, 7, 8, 9]),
            collect([9, 8, 9, 9, 9, 6, 5, 6, 7, 8]),
        ]);
        $this->topLeft = new ContourPoint(0, [0, 0]);
        $this->topRight = new ContourPoint(0, [9, 0]);
        $this->bottomLeft = new ContourPoint(9, [0, 4]);
        $this->bottomRight = new ContourPoint(8, [9, 4]);
    }

    // Tests for the LcdDisplay class
    /** @test */
    public function can_split_input_into_grid_of_integers()
    {
        $input = $this->getInputAsCollection();
        $contourMap = new ContourMap($input);

        $this->assertEquals($this->testGrid, $contourMap->map);

        $this->assertTrue(6 === $contourMap->map[3][2]);
        $this->assertTrue(2 === $contourMap->map[0][0]);
        $this->assertTrue(8 === $contourMap->map[4][9]);
    }
    /** @test */
    public function can_get_correct_comparators_for_all_corners()
    {
        $input = $this->getInputAsCollection();
        $contourMap = new ContourMap($input);

        // Top left corner
        $this->assertEquals(
            collect([
                new ContourPoint(9),
                new ContourPoint(9),
                new ContourPoint(1, [1, 0]),
                new ContourPoint(3, [0, 1]),
            ]),
            $contourMap->comparatorsFor($this->topLeft)
        );

        // Top right corner
        $this->assertEquals(
            collect([
                new ContourPoint(9),
                new ContourPoint(1, [8, 0]),
                new ContourPoint(9),
                new ContourPoint(1, [9, 1]),
            ]),
            $contourMap->comparatorsFor($this->topRight)
        );

        // Bottom left corner
        $this->assertEquals(
            collect([
                new ContourPoint(8, [0, 3]),
                new ContourPoint(9),
                new ContourPoint(8, [1, 4]),
                new ContourPoint(9),
            ]),
            $contourMap->comparatorsFor($this->bottomLeft)
        );

        // Bottom right corner
        $this->assertEquals(
            collect([
                new ContourPoint(9, [9, 3]),
                new ContourPoint(7, [8, 4]),
                new ContourPoint(9),
                new ContourPoint(9),
            ]),
            $contourMap->comparatorsFor($this->bottomRight)
        );
    }
    /** @test */
    public function can_get_correct_comparators_for_top_and_bottom_row()
    {
        $input = $this->getInputAsCollection();
        $contourMap = new ContourMap($input);

        // Top row
        $this->assertEquals(collect([
                new ContourPoint(9),
                new ContourPoint(9, [3, 0]),
                new ContourPoint(4, [5, 0]),
                new ContourPoint(8, [4, 1]),
            ]),
            $contourMap->comparatorsFor(new ContourPoint(9, [4, 0]))
        );

        // Bottom row
        $this->assertEquals(collect([
                new ContourPoint(7, [7, 3]),
                new ContourPoint(5, [6, 4]),
                new ContourPoint(7, [8, 4]),
                new ContourPoint(9),
            ]),
            $contourMap->comparatorsFor(new ContourPoint(6, [7, 4]))
        );
    }
    /** @test */
    public function can_get_correct_comparators_for_middle_of_the_grid()
    {
        $input = $this->getInputAsCollection();
        $contourMap = new ContourMap($input);

        // Top row
        $this->assertEquals(collect([
                new ContourPoint(5, [2, 2]),
                new ContourPoint(7, [1, 3]),
                new ContourPoint(7, [3, 3]),
                new ContourPoint(9, [2, 4]),
            ]),
            $contourMap->comparatorsFor(new ContourPoint(6, [2, 3]))
        );

        // Bottom row
        $this->assertEquals(collect([
            new ContourPoint(9, [6, 2]),
            new ContourPoint(9, [5, 3]),
            new ContourPoint(7, [7, 3]),
            new ContourPoint(5, [6, 4]),
        ]),
            $contourMap->comparatorsFor(new ContourPoint(6, [6, 3]))
        );
    }
    /** @test */
    public function can_tell_if_a_corner_point_is_low_point()
    {
        $input = $this->getInputAsCollection();
        $contourMap = new ContourMap($input);

        $point = new ContourPoint(5, [6, 3]);
        $comparators = collect([
            new ContourPoint(9),
            new ContourPoint(9),
            new ContourPoint(6),
            new ContourPoint(7),
        ]);
        $this->assertTrue($contourMap->isLowPoint($point, $comparators));
        $comparators = collect([
            new ContourPoint(9),
            new ContourPoint(5),
            new ContourPoint(9),
            new ContourPoint(7),
        ]);
        $this->assertFalse($contourMap->isLowPoint($point, $comparators));
        $comparators = collect([
            new ContourPoint(9),
            new ContourPoint(5),
            new ContourPoint(9),
            new ContourPoint(5),
        ]);
        $this->assertFalse($contourMap->isLowPoint($point, $comparators));
    }
    /** @test */
    public function can_tell_if_a_mid_grid_point_is_low_point()
    {
        $input = $this->getInputAsCollection();
        $contourMap = new ContourMap($input);

        $point = new ContourPoint(5, [6, 3]);
        $comparators = collect([
            new ContourPoint(9),
            new ContourPoint(6),
            new ContourPoint(6),
            new ContourPoint(7),
        ]);
        $this->assertTrue($contourMap->isLowPoint($point, $comparators));
        $comparators = collect([
            new ContourPoint(5),
            new ContourPoint(5),
            new ContourPoint(5),
            new ContourPoint(5),
        ]);
        $this->assertFalse($contourMap->isLowPoint($point, $comparators));
        $comparators = collect([
            new ContourPoint(4),
            new ContourPoint(1),
            new ContourPoint(7),
            new ContourPoint(8),
        ]);
        $this->assertFalse($contourMap->isLowPoint($point, $comparators));
    }
    /** @test */
    public function can_find_all_points_in_a_basin()
    {
        $input = $this->getInputAsCollection();
        $contourMap = new ContourMap($input);
        $contourMap->mapBasins();

        $this->assertEquals(
            collect([
                new ContourPoint(1, [1, 0]),
                new ContourPoint(2, [0, 0]),
                new ContourPoint(3, [0, 1]),
            ]),
            $contourMap->basins[0]->points
        );
        $this->assertEquals(3, $contourMap->basins[0]->getSize());

        $this->assertEquals(
            collect([
                new ContourPoint(0, [9, 0]),
                new ContourPoint(1, [8, 0]),
                new ContourPoint(2, [7, 0]),
                new ContourPoint(3, [6, 0]),
                new ContourPoint(4, [5, 0]),
                new ContourPoint(4, [6, 1]),
                new ContourPoint(2, [8, 1]),
                new ContourPoint(1, [9, 1]),
                new ContourPoint(2, [9, 2]),
            ]),
            $contourMap->basins[1]->points
        );
        $this->assertEquals(9, $contourMap->basins[1]->getSize());

        $this->assertEquals(
            collect([
                new ContourPoint(5, [2, 2]),
                new ContourPoint(8, [2, 1]),
                new ContourPoint(7, [3, 1]),
                new ContourPoint(8, [4, 1]),
                new ContourPoint(7, [4, 2]),
                new ContourPoint(6, [3, 2]),
                new ContourPoint(7, [3, 3]),
                new ContourPoint(6, [2, 3]),
                new ContourPoint(7, [1, 3]),
                new ContourPoint(8, [1, 2]),
                new ContourPoint(8, [0, 3]),
                new ContourPoint(8, [1, 4]),
                new ContourPoint(8, [4, 3]),
                new ContourPoint(8, [5, 2]),
            ]),
            $contourMap->basins[2]->points
        );
        $this->assertEquals(14, $contourMap->basins[2]->getSize());

        $this->assertEquals(
            collect([
                new ContourPoint(5, [6, 4]),
                new ContourPoint(6, [6, 3]),
                new ContourPoint(7, [7, 3]),
                new ContourPoint(8, [7, 2]),
                new ContourPoint(8, [8, 3]),
                new ContourPoint(7, [8, 4]),
                new ContourPoint(6, [7, 4]),
                new ContourPoint(8, [9, 4]),
                new ContourPoint(6, [5, 4]),
            ]),
            $contourMap->basins[3]->points
        );
        $this->assertEquals(9, $contourMap->basins[3]->getSize());
    }



    // Tests for the main day class
    /** @test */
    public function find_all_low_points()
    {
        $day = new Day_09('day_09_test.txt');

        $this->assertEquals(collect([1, 0, 5, 5]), $day->map->lowPoints());
    }
    /** @test */
    public function can_get_correct_test_result_for_part_one()
    {
        $day = new Day_09('day_09_test.txt');

        $this->assertEquals(15, $day->partOne());
    }
    /** @test */
    public function can_get_correct_test_result_for_part_two()
    {
        $day = new Day_09('day_09_test.txt');

        $this->assertEquals(1134, $day->partTwo());
    }
}