<?php namespace Tests\Days;

use App\Lib\Classes\Days\Day_05;
use App\Lib\Classes\Grid\Grid;
use App\Lib\Classes\Grid\Line;
use App\Lib\Classes\Grid\Point;
use PHPUnit\Framework\TestCase;

class Day05Test extends TestCase
{
    protected Day_05 $day;

    // Tests for Point class
    /** @test */
    public function can_get_new_point_from_string()
    {
        $point = Point::getPointFromString("0, 4");

        $this->assertTrue(0 === $point->x);
        $this->assertTrue(4 === $point->y);
        $this->assertEquals(collect([0,4]), $point->coordinates);
    }
    /** @test */
    public function can_get_new_point_from_array()
    {
        $point = Point::getPointFromArray([0, 4]);

        $this->assertTrue(0 === $point->x);
        $this->assertTrue(4 === $point->y);
        $this->assertEquals(collect([0,4]), $point->coordinates);
    }
    /** @test */
    public function can_get_correctly_ordered_horizontal_points()
    {
        $point1 = Point::getPointFromString("0, 4");
        $point2 = Point::getPointFromString("5, 4");

        list($first, $last) = Point::orderPoints($point1, $point2);

        $this->assertEquals(0, $first->x);
        $this->assertEquals(4, $first->y);
        $this->assertEquals(5, $last->x);
        $this->assertEquals(4, $last->y);

        list($first, $last) = Point::orderPoints($point2, $point1);

        $this->assertEquals(0, $first->x);
        $this->assertEquals(4, $first->y);
        $this->assertEquals(5, $last->x);
        $this->assertEquals(4, $last->y);
    }
    /** @test */
    public function can_get_correctly_ordered_vertical_points()
    {
        $point1 = Point::getPointFromString("3, 9");
        $point2 = Point::getPointFromString("3, 2");

        list($first, $last) = Point::orderPoints($point1, $point2);

        $this->assertEquals(3, $first->x);
        $this->assertEquals(2, $first->y);
        $this->assertEquals(3, $last->x);
        $this->assertEquals(9, $last->y);

        list($first, $last) = Point::orderPoints($point2, $point1);

        $this->assertEquals(3, $first->x);
        $this->assertEquals(2, $first->y);
        $this->assertEquals(3, $last->x);
        $this->assertEquals(9, $last->y);
    }
    /** @test */
    public function can_get_correctly_ordered_diagonal_points()
    {
        $point1 = Point::getPointFromString("5, 5");
        $point2 = Point::getPointFromString("0, 0");

        list($first, $last) = Point::orderPoints($point1, $point2);

        $this->assertEquals(0, $first->x);
        $this->assertEquals(0, $first->y);
        $this->assertEquals(5, $last->x);
        $this->assertEquals(5, $last->y);

        list($first, $last) = Point::orderPoints($point2, $point1);

        $this->assertEquals(0, $first->x);
        $this->assertEquals(0, $first->y);
        $this->assertEquals(5, $last->x);
        $this->assertEquals(5, $last->y);

        $point1 = Point::getPointFromString("2, 8");
        $point2 = Point::getPointFromString("7, 3");

        list($first, $last) = Point::orderPoints($point1, $point2);

        $this->assertEquals(2, $first->x);
        $this->assertEquals(8, $first->y);
        $this->assertEquals(7, $last->x);
        $this->assertEquals(3, $last->y);

        list($first, $last) = Point::orderPoints($point2, $point1);

        $this->assertEquals(2, $first->x);
        $this->assertEquals(8, $first->y);
        $this->assertEquals(7, $last->x);
        $this->assertEquals(3, $last->y);
    }


    // Tests for line class
    /** @test */
    public function can_create_new_line_with_start_and_end_points()
    {
        $line = new Line("0,9 -> 5,9");

        $this->assertEquals(collect([0,9]), $line->start->coordinates);
        $this->assertEquals(collect([5,9]), $line->end->coordinates);
    }
    /** @test */
    public function can_create_new_ordered_line_with_reversed_start_and_end_points()
    {
        $line = new Line("5,9 -> 0,9");

        $this->assertEquals(collect([0,9]), $line->start->coordinates);
        $this->assertEquals(collect([5,9]), $line->end->coordinates);
    }
    /** @test */
    public function new_line_calculates_vertical_line_type_correctly()
    {
        $line = new Line("1,1 -> 1,3");

        $this->assertEquals('vertical', $line->type);
    }
    /** @test */
    public function new_line_calculates_horizontal_line_type_correctly()
    {
        $line = new Line("0,9 -> 5,9");

        $this->assertEquals('horizontal', $line->type);
    }
    /** @test */
    public function new_line_calculates_diagonal_line_type_correctly()
    {
        $line = new Line("6,4 -> 2,0");

        $this->assertEquals('diagonal', $line->type);
    }
    /** @test */
    public function new_line_calculates_all_points_on_vertical_line_correctly()
    {
        $points = collect([
            Point::getPointFromArray([1, 1]),
            Point::getPointFromArray([1, 2]),
            Point::getPointFromArray([1, 3]),
        ]);
        $line = new Line("1,1 -> 1,3");

        $this->assertEquals($points, $line->points);
    }
    /** @test */
    public function new_line_calculates_all_points_on_horizontal_line_correctly()
    {
        $points = collect([
            Point::getPointFromArray([0, 9]),
            Point::getPointFromArray([1, 9]),
            Point::getPointFromArray([2, 9]),
            Point::getPointFromArray([3, 9]),
            Point::getPointFromArray([4, 9]),
            Point::getPointFromArray([5, 9]),
        ]);
        $line = new Line("0,9 -> 5,9");

        $this->assertEquals($points, $line->points);
    }
    /** @test */
    public function new_line_calculates_all_points_on_diagonal_line_correctly()
    {
        // Line increasing left to right
        $points = collect([
            Point::getPointFromArray([2, 8]),
            Point::getPointFromArray([3, 7]),
            Point::getPointFromArray([4, 6]),
            Point::getPointFromArray([5, 5]),
            Point::getPointFromArray([6, 4]),
            Point::getPointFromArray([7, 3]),
        ]);
        $line = new Line("7,3 -> 2,8");

        $this->assertEquals($points, $line->points);

        // Line decreasing left to right
        $points = collect([
            Point::getPointFromArray([0, 0]),
            Point::getPointFromArray([1, 1]),
            Point::getPointFromArray([2, 2]),
            Point::getPointFromArray([3, 3]),
            Point::getPointFromArray([4, 4]),
            Point::getPointFromArray([5, 5]),
        ]);
        $line = new Line("0,0 -> 5,5");

        $this->assertEquals($points, $line->points);
    }


    // Tests for the grid class
    /** @test */
    public function can_build_new_empty_2_grid()
    {
        $grid = new Grid(2);
        $blankGrid = [
            [0,0],
            [0,0],
        ];

        $this->assertEquals($blankGrid, $grid->render());
    }
    /** @test */
    public function can_build_new_empty_10_grid()
    {
        $grid = new Grid(10);
        $blankGrid = [
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
        ];

        $this->assertEquals($blankGrid, $grid->render());
    }
    /** @test */
    public function correct_grid_after_adding_new_line()
    {
        $grid = new Grid(10);
        $grid->update(new Line("5,9 -> 0,9"));

        $expected = [
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [1,1,1,1,1,1,0,0,0,0],
        ];

        $this->assertEquals($expected, $grid->render());
    }
    /** @test */
    public function correct_grid_after_adding_overlapping_lines()
    {
        $grid = new Grid(10);
        $grid->update(new Line("5,9 -> 0,9"));
        $grid->update(new Line("0,9 -> 2,9"));

        $expected = [
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [2,2,2,1,1,1,0,0,0,0],
        ];

        $this->assertEquals($expected, $grid->render());
    }
    /** @test */
    public function calculate_correct_overlaps_on_empty_grid()
    {
        $grid = new Grid(10);

        $this->assertEquals(0, $grid->calculateOverlap());
    }
    /** @test */
    public function calculate_correct_overlaps_on_grid_with_no_overlaps()
    {
        $grid = new Grid(10);
        $grid->setGrid([
            [0,0,0,0,0,0,0,0,0,0],
            [1,1,1,1,1,1,1,1,1,1],
            [0,0,1,1,1,0,0,0,0,0],
            [0,0,0,1,0,0,0,0,0,0],
            [0,0,0,1,0,0,0,0,0,0],
            [0,0,0,1,0,0,0,0,0,0],
            [0,0,0,1,0,0,0,0,0,0],
            [0,0,0,1,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
        ]);

        $this->assertEquals(0, $grid->calculateOverlap());
    }
    /** @test */
    public function calculate_correct_overlaps_on_grid_with_overlaps_of_order_2()
    {
        $grid = new Grid(10);
        $grid->setGrid([
            [0,1,0,0,0,0,0,0,0,0],
            [1,2,1,1,1,1,1,1,1,1],
            [0,1,1,1,1,0,0,0,0,0],
            [0,1,0,2,0,0,0,0,0,0],
            [0,0,0,2,0,0,0,0,0,0],
            [0,0,0,2,0,0,0,0,0,0],
            [0,0,0,1,0,0,0,0,0,0],
            [0,0,0,1,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0],
        ]);

        $this->assertEquals(4, $grid->calculateOverlap());
    }
    /** @test */
    public function calculate_correct_overlaps_on_grid_with_overlaps_of_order_3()
    {
        $grid = new Grid(10);
        $grid->setGrid([
            [0,1,0,0,0,0,0,0,0,0],
            [1,2,1,1,1,1,1,1,1,1],
            [0,1,1,1,1,0,0,0,0,0],
            [0,1,0,2,0,0,0,0,0,0],
            [0,0,0,2,0,0,0,0,0,0],
            [0,0,0,2,1,0,0,0,0,0],
            [0,0,1,3,1,0,0,0,0,0],
            [0,1,3,1,0,0,0,0,0,0],
            [1,1,1,1,0,0,0,0,0,0],
            [0,1,0,0,0,0,0,0,0,0],
        ]);

        $this->assertEquals(6, $grid->calculateOverlap());
    }


    // Tests for main day class
    /** @test */
    public function day_object_gets_correct_lines()
    {
        $this->day = new Day_05('day_05_test.txt', 10);
        $line2 = collect([
            Point::getPointFromArray([3, 4]),
            Point::getPointFromArray([4, 4]),
            Point::getPointFromArray([5, 4]),
            Point::getPointFromArray([6, 4]),
            Point::getPointFromArray([7, 4]),
            Point::getPointFromArray([8, 4]),
            Point::getPointFromArray([9, 4]),
        ]);
        $line4 = collect([
            Point::getPointFromArray([7, 0]),
            Point::getPointFromArray([7, 1]),
            Point::getPointFromArray([7, 2]),
            Point::getPointFromArray([7, 3]),
            Point::getPointFromArray([7, 4]),
        ]);

        $this->assertEquals(10, $this->day->lines->count());
        $this->assertEquals($line2, $this->day->lines[2]->points);
        $this->assertEquals($line4, $this->day->lines[4]->points);
    }
    /** @test */
    public function can_get_correct_answer_for_test_data_part_one()
    {
        $this->day = new Day_05('day_05_test.txt', 10);
        $this->assertEquals(5, $this->day->partOne());
    }
    /** @test */
    public function can_get_correct_answer_for_test_data_part_two()
    {
        $this->day = new Day_05('day_05_test.txt', 10);
        $this->assertEquals(12, $this->day->partTwo());
    }
}