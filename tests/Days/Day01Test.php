<?php

namespace Tests\Days;
use App\Lib\Classes\Days\Day_01;
use PHPUnit\Framework\TestCase;

class Day01Test extends TestCase
{
    /** @test */
    public function correct_increase_for_simple_increasing_data()
    {
        $day01 = new Day_01('day_01_test.txt');

        $this->assertEquals(3, $day01->getIncrease(collect([1,2,3,4])));
    }
    /** @test */
    public function correct_increase_for_simple_decreasing_data()
    {
        $day01 = new Day_01('day_01_test.txt');

        $this->assertEquals(0, $day01->getIncrease(collect([4,3,2,1,0])));
    }
    /** @test */
    public function correct_increase_for_flat_data_input()
    {
        $day01 = new Day_01('day_01_test.txt');

        $this->assertEquals(0, $day01->getIncrease(collect([4,4,4,4,4,4,4])));
    }
    /** @test */
    public function can_get_correct_increases_for_test_data()
    {
        $day01 = new Day_01('day_01_test.txt');

        $this->assertEquals(7, $day01->partOne());
    }
    /** @test */
    public function can_get_correct_answer_for_3d_test_data()
    {
        $day01 = new Day_01('day_01_test.txt');

        $this->assertEquals(5, $day01->partTwo());
    }
}