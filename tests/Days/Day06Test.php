<?php namespace Tests\Days;

use App\Lib\Classes\Days\Day_06;
use App\Lib\Classes\LanternfishPopulation;
use PHPUnit\Framework\TestCase;

class Day06Test extends TestCase
{
    protected Day_06 $day;

    // Tests for LanternfishPopulation class
    /** @test */
    public function can_add_lanternfish_to_population()
    {
        $population = new LanternfishPopulation();
        $this->assertEquals(0, $population->count());

        $population->add(3);

        $this->assertEquals(1, $population->count());
    }
    /** @test */
    public function can_refresh_lanternfish_population()
    {
        $population = new LanternfishPopulation("3,4,5,6,0");

        $this->assertEquals(5, $population->count());

        $population->refresh();
        $this->assertEquals(0, $population->count());
    }
    /** @test */
    public function can_get_correct_population_count_status_over_ten_days()
    {
        $population = new LanternfishPopulation("4,3,4,2,1");

        $this->assertEquals(5, $population->count());
        $population->newDay();
        $this->assertEquals(5, $population->count());
        $population->newDay();
        $this->assertEquals(6, $population->count());
        $population->newDay();
        $this->assertEquals(7, $population->count());
        $population->newDay();
        $this->assertEquals(8, $population->count());
        $population->newDay();
        $this->assertEquals(10, $population->count());
        $population->newDay();
        $this->assertEquals(10, $population->count());
        $population->newDay();
        $this->assertEquals(10, $population->count());
        $population->newDay();
        $this->assertEquals(10, $population->count());
        $population->newDay();
        $this->assertEquals(11, $population->count());
    }


    // Tests for the main day class
    /** @test */
    public function can_populate_day_with_correct_population()
    {
        $day = new Day_06('day_06_test.txt');

        $this->assertEquals(5, $day->population->count());
        $this->assertEquals(collect([0,1,1,2,1,0,0,0,0]), $day->population->fish);
    }
    /** @test */
    public function can_get_correct_population_count_for_part_one()
    {
        $day = new Day_06('day_06_test.txt');

        $day->population->newDay(18);
        $this->assertEquals(26, $day->population->count());

        $day->population->newDay(62);
        $this->assertEquals(5934, $day->population->count());
    }
    /** @test */
    public function can_get_correct_report_for_part_two()
    {
        $day = new Day_06('day_06_test.txt');

        $day->population->newDay(256);
        $this->assertEquals(26984457539, $day->population->count());
    }
}