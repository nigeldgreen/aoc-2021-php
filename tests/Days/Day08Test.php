<?php namespace Tests\Days;

use App\Lib\Classes\Days\Day_08;
use App\Lib\Classes\LcdDisplay;
use PHPUnit\Framework\TestCase;

class Day08Test extends TestCase
{
    protected Day_08 $day;

    // Tests for the LcdDisplay class
    /** @test */
    public function can_split_input_line_into_inputs_and_outputs()
    {
        $display = new LcdDisplay("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf");

        $this->assertEquals(collect([
            "abcdefg",
            "bcdef",
            "acdfg",
            "abcdf",
            "abd",
            "abcdef",
            "bcdefg",
            "abef",
            "abcdeg",
            "ab",
        ]), $display->inputs);
        $this->assertEquals(collect([
            "bcdef",
            "abcdf",
            "bcdef",
            "abcdf",
        ]), $display->outputs);
    }
    /** @test */
    public function can_calculate_zero_unique_outputs()
    {
        $display = new LcdDisplay("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf");

        $this->assertEquals(0, $display->uniqueNumberOutputs());
    }
    /** @test */
    public function can_calculate_one_unique_outputs()
    {
        $display = new LcdDisplay("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | edf fcadb cdfeb cdbaf");

        $this->assertEquals(1, $display->uniqueNumberOutputs());
    }
    /** @test */
    public function can_calculate_two_unique_outputs()
    {
        $display = new LcdDisplay("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | edf fcadb cd cdbaf");

        $this->assertEquals(2, $display->uniqueNumberOutputs());
    }
    /** @test */
    public function can_calculate_three_unique_outputs()
    {
        $display = new LcdDisplay("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | edf egcd cd cdbaf");

        $this->assertEquals(3, $display->uniqueNumberOutputs());
    }
    /** @test */
    public function can_calculate_four_unique_outputs()
    {
        $display = new LcdDisplay("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | edf egcd cd abcdefg");

        $this->assertEquals(4, $display->uniqueNumberOutputs());
    }


    /** @test */
    public function can_calculate_correct_inputs()
    {
        $display = new LcdDisplay("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf");

        $display->mapOutputs();

        $this->assertEquals(collect([
            'a' => 'd',
            'b' => 'e',
            'c' => 'a',
            'd' => 'f',
            'e' => 'g',
            'f' => 'b',
            'g' => 'c',
        ]), $display->mappedInputs);
    }


    /** @test */
    public function can_get_correct_output_numbers()
    {
        $display = new LcdDisplay("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf");

        $display->mapOutputs();

        $this->assertEquals(5353, $display->getTotalOutput());
    }



    // Tests for the main day class
    /** @test */
    public function new_test_day_initialised_with_10_displays()
    {
        $day = new Day_08('day_08_test.txt');

        $this->assertEquals(10, $day->displays->count());
    }
    /** @test */
    public function can_get_correct_test_result_for_part_one()
    {
        $day = new Day_08('day_08_test.txt');

        $this->assertEquals(26, $day->partOne());
    }
    /** @test */
    public function can_get_correct_test_result_for_part_two()
    {
        $day = new Day_08('day_08_test.txt');

        $this->assertEquals(61229, $day->partTwo());
    }
}