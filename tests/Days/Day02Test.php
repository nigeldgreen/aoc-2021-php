<?php

namespace Tests\Days;
use App\Lib\Classes\Days\Day_02;
use PHPUnit\Framework\TestCase;

class Day02Test extends TestCase
{
    protected Day_02 $day;

    public function setUp() : void
    {
        $this->day = new Day_02('day_02_test.txt');
    }
    /** @test */
    public function can_get_correct_answer_for_test_data_part_one()
    {
        $this->assertEquals(150, $this->day->partOne());
    }
    /** @test */
    public function can_get_correct_answer_for_test_data_part_two()
    {
        $this->assertEquals(900, $this->day->partTwo());
    }
}