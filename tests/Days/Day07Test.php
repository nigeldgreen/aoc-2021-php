<?php namespace Tests\Days;

use App\Lib\Classes\CrabFleet;
use App\Lib\Classes\Days\Day_07;
use PHPUnit\Framework\TestCase;

class Day07Test extends TestCase
{
    protected Day_07 $day;

    // Tests for the CrabFleet class
    /** @test */
    public function can_get_correct_starting_positions_for_fleet_from_input()
    {
        $fleet = new CrabFleet("16,1,2,0,4,2,7,1,2,14");

        $this->assertEquals(collect([0,1,1,2,2,2,4,7,14,16]), $fleet->occupiedPositions);
    }
    /** @test */
    public function can_get_correct_total_positions_for_fleet_from_input()
    {
        $fleet = new CrabFleet("16,1,2,0,4,2,7,1,2,14");

        $this->assertEquals(collect([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]), $fleet->allPositions);
    }
    /** @test */
    public function can_get_correct_possible_cumulative_sums()
    {
        $fleet = new CrabFleet("16,1,2,0,4,2,7,1,2,14");

        $this->assertEquals([
            0 => 0,
            1 => 1,
            2 => 3,
            3 => 6,
            4 => 10,
            5 => 15,
            6 => 21,
            7 => 28,
            8 => 36,
            9 => 45,
            10 => 55,
            11 => 66,
            12 => 78,
            13 => 91,
            14 => 105,
            15 => 120,
            16 => 136,
        ], $fleet->cumulativeSums);
    }
    /** @test */
    public function can_calculate_total_fuel_cost_of_linear_fleet_move()
    {
        $fleet = new CrabFleet("2,3,5,6,9");

        $this->assertEquals(15, $fleet->fuelCostOfJump(2, false));
        $this->assertEquals(12, $fleet->fuelCostOfJump(3, false));
        $this->assertEquals(10, $fleet->fuelCostOfJump(5, false));
        $this->assertEquals(11, $fleet->fuelCostOfJump(6, false));
        $this->assertEquals(20, $fleet->fuelCostOfJump(9, false));
    }
    /** @test */
    public function can_calculate_total_fuel_cost_of_exponential_fleet_move()
    {
        $fleet = new CrabFleet("2,3,5,6,9");

        $this->assertEquals(45, $fleet->fuelCostOfJump(2, true));
        $this->assertEquals(31, $fleet->fuelCostOfJump(3, true));
        $this->assertEquals(20, $fleet->fuelCostOfJump(5, true));
        $this->assertEquals(23, $fleet->fuelCostOfJump(6, true));
        $this->assertEquals(65, $fleet->fuelCostOfJump(9, true));
    }
    /** @test */
    public function can_calculate_most_efficient_position_for_fleet()
    {
        $fleet = new CrabFleet("2,3,5,6,9");

        $fleet->calculateMostEfficientPositionLinear();

        $this->assertEquals(10, $fleet->mostEfficientFuelCost);
        $this->assertEquals(5, $fleet->mostEfficientPosition);
    }



    // Tests for the main day class
    /** @test */
    public function can_get_correct_test_result_for_part_one()
    {
        $day = new Day_07('day_07_test.txt');
        $this->assertEquals(
            "Most efficient position is position 2 which has a fuel cost of 37",
            $day->partOne()
        );
    }
    /** @test */
    public function can_get_correct_test_result_for_part_two()
    {
        $day = new Day_07('day_07_test.txt');
        $this->assertEquals(
            "Most efficient position is position 5 which has a fuel cost of 168",
            $day->partTwo()
        );
    }
}