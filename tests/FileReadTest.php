<?php

namespace Tests;
use App\Lib\Classes\FileHandler;
use PHPUnit\Framework\TestCase;

class FileReadTest extends TestCase
{
    /** @test */
    public function can_get_correct()
    {
        $reader = new FileHandler('../input/day_01_test.txt');
        $this->assertEquals(
            ['1721', '979', '366', '299', '675', '1456'],
            $reader->getInputAsCollection()
        );
    }
}